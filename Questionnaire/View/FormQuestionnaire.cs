﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Questionnaire
{
    /// <summary>
    /// Description:    FormQuestionnaire shows the questions and the answers
    ///                 and is the master of the 3-tier-architecture
    /// Kata:           https://app.box.com/s/3clcy04py9e1ngfslilv
    /// CCD:            Integration and Operation
    /// Author:         Dr. Ehlert / OSZ IMT 
    /// Date:           April 2015 / Version2: Mai 2015
    /// </summary>
    
    public partial class FormQuestionnaire : Form
    {
        private IQuestionnaireHandler handler;
        RadioButton[] radioButtons;
              
        public FormQuestionnaire(IQuestionnaireHandler handler)
        {
            this.handler = handler;                         // reference to the business logic layer
            InitializeComponent();
            radioButtonAnswer1.AutoCheck = true;            // hold the focus, fixing a bug of the form designer
            buttonShowMyScore.Enabled = false;              // requirements definition
            radioButtons = new RadioButton[4];              // requirements definition: maxRadioButtons == 4
            InitializeRadioButtons();
            FillView(handler.GetQuestionWithAnswers());     // first question
        }

        private void InitializeRadioButtons()
        {
            // requirements definition: maxRadioButtons == 4
            radioButtons[0] = radioButtonAnswer1;
            radioButtons[1] = radioButtonAnswer2;
            radioButtons[2] = radioButtonAnswer3;
            radioButtons[3] = radioButtonAnswer4;
        }

        private void ResetRadioButtons()
        {
            for (int i = 0; i < radioButtons.Length; i++)
                radioButtons[i].Checked = false;
        }

        private void FillView(QuestionWithAnswers questionWithAnswers)
        {
            if (!handler.IsSolved())
                ResetRadioButtons();
            else
                radioButtons[handler.GetGivenAnswerNr()].Checked = true;
            labelQuestion.Text = questionWithAnswers.QuestionText;
            for (int i = 0; i < radioButtons.Length; i++)
                radioButtons[i].Text = questionWithAnswers.ListAnswers.ElementAt(i);
        }

        private int GetAnswerNr()
        {
            for (int i = 0; i < radioButtons.Length; i++)
                if (radioButtons[i].Checked) 
                    return i;
            return -1;
        }

        private void buttonForward_Click(object sender, EventArgs e)
        {
            FillView(handler.GetForwardQuestion());
        }

        private void buttonBackward_Click(object sender, EventArgs e)
        {
            FillView(handler.GetBackwardQuestion());
        }

        private void buttonShowMyScore_Click(object sender, EventArgs e)
        {            
            new FormPercent(handler).ShowDialog();
        }
        
        private void radioButton_ClickEvent(object sender, MouseEventArgs e)
        {
            handler.SetAnswer(GetAnswerNr());
            buttonShowMyScore.Enabled = handler.IsQuestionnaireFullSolved();
        }

        private void FormQuestionnaire_Load(object sender, EventArgs e)
        {

        }


    }
}
