﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Questionnaire
{
    /// <summary>
    /// Description:    FormPercent shows the result
    /// Kata:           https://app.box.com/s/3clcy04py9e1ngfslilv 
    /// Author:         Dr. Ehlert / OSZ IMT
    /// Date:           April 2015 / Version2: Mai 2015
    /// </summary>
    
    public partial class FormPercent : Form
    {
        public FormPercent(IQuestionnaireHandler handler)
        {
           
            InitializeComponent();
            
            for (int questionNr = 0; questionNr < handler.CountQuestions(); questionNr++)
            {
                if (questionNr == 0)
                {
                    handler.Reset();
                    listView.Items.Add(handler.GetQuestionWithAnswers().QuestionText);
                }
                else
                    listView.Items.Add(handler.GetForwardQuestion().QuestionText);
                if (handler.IsAnswerCorrect())
                {
                    listView.Items.Add("   Your answer '" + handler.GetAnswer(handler.GetGivenAnswerNr()) + "' is correct");
                }
                else
                {
                    listView.Items.Add("   Your answer '" + handler.GetAnswer(handler.GetGivenAnswerNr()) + "' is wrong");
                    listView.Items.Add("   Correct Answer: " + handler.GetAnswer(handler.GetCorrectAnswerNr()));
                }
            }
            labelPercent.Text = handler.CountCorrectAnswers() + " out of " + handler.CountQuestions() + " questions answered correctly (" + handler.GetPercentOfCorrectAnswers() + "%)";
        }

        private void FormPercent_Load(object sender, EventArgs e)
        {
            
        }

    }
}
