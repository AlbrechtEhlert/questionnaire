﻿namespace Questionnaire
{
    partial class FormQuestionnaire
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelQuestion = new System.Windows.Forms.Panel();
            this.buttonForward = new System.Windows.Forms.Button();
            this.buttonBackward = new System.Windows.Forms.Button();
            this.radioButtonAnswer4 = new System.Windows.Forms.RadioButton();
            this.radioButtonAnswer3 = new System.Windows.Forms.RadioButton();
            this.radioButtonAnswer2 = new System.Windows.Forms.RadioButton();
            this.radioButtonAnswer1 = new System.Windows.Forms.RadioButton();
            this.labelQuestion = new System.Windows.Forms.Label();
            this.buttonShowMyScore = new System.Windows.Forms.Button();
            this.panelQuestion.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelQuestion
            // 
            this.panelQuestion.Controls.Add(this.buttonForward);
            this.panelQuestion.Controls.Add(this.buttonBackward);
            this.panelQuestion.Controls.Add(this.radioButtonAnswer4);
            this.panelQuestion.Controls.Add(this.radioButtonAnswer3);
            this.panelQuestion.Controls.Add(this.radioButtonAnswer2);
            this.panelQuestion.Controls.Add(this.radioButtonAnswer1);
            this.panelQuestion.Controls.Add(this.labelQuestion);
            this.panelQuestion.Location = new System.Drawing.Point(75, 53);
            this.panelQuestion.Name = "panelQuestion";
            this.panelQuestion.Size = new System.Drawing.Size(391, 327);
            this.panelQuestion.TabIndex = 0;
            // 
            // buttonForward
            // 
            this.buttonForward.Location = new System.Drawing.Point(168, 270);
            this.buttonForward.Name = "buttonForward";
            this.buttonForward.Size = new System.Drawing.Size(75, 23);
            this.buttonForward.TabIndex = 7;
            this.buttonForward.Text = "Forward";
            this.buttonForward.UseVisualStyleBackColor = true;
            this.buttonForward.Click += new System.EventHandler(this.buttonForward_Click);
            // 
            // buttonBackward
            // 
            this.buttonBackward.Location = new System.Drawing.Point(40, 270);
            this.buttonBackward.Name = "buttonBackward";
            this.buttonBackward.Size = new System.Drawing.Size(75, 23);
            this.buttonBackward.TabIndex = 6;
            this.buttonBackward.Text = "Backward";
            this.buttonBackward.UseVisualStyleBackColor = true;
            this.buttonBackward.Click += new System.EventHandler(this.buttonBackward_Click);
            // 
            // radioButtonAnswer4
            // 
            this.radioButtonAnswer4.AutoSize = true;
            this.radioButtonAnswer4.Location = new System.Drawing.Point(40, 218);
            this.radioButtonAnswer4.Name = "radioButtonAnswer4";
            this.radioButtonAnswer4.Size = new System.Drawing.Size(66, 17);
            this.radioButtonAnswer4.TabIndex = 4;
            this.radioButtonAnswer4.TabStop = true;
            this.radioButtonAnswer4.Text = "Answer4";
            this.radioButtonAnswer4.UseVisualStyleBackColor = true;
            this.radioButtonAnswer4.MouseClick += new System.Windows.Forms.MouseEventHandler(this.radioButton_ClickEvent);
            // 
            // radioButtonAnswer3
            // 
            this.radioButtonAnswer3.AutoSize = true;
            this.radioButtonAnswer3.Location = new System.Drawing.Point(40, 176);
            this.radioButtonAnswer3.Name = "radioButtonAnswer3";
            this.radioButtonAnswer3.Size = new System.Drawing.Size(66, 17);
            this.radioButtonAnswer3.TabIndex = 3;
            this.radioButtonAnswer3.TabStop = true;
            this.radioButtonAnswer3.Text = "Answer3";
            this.radioButtonAnswer3.UseVisualStyleBackColor = true;
            this.radioButtonAnswer3.MouseClick += new System.Windows.Forms.MouseEventHandler(this.radioButton_ClickEvent);
            // 
            // radioButtonAnswer2
            // 
            this.radioButtonAnswer2.AutoSize = true;
            this.radioButtonAnswer2.Location = new System.Drawing.Point(40, 130);
            this.radioButtonAnswer2.Name = "radioButtonAnswer2";
            this.radioButtonAnswer2.Size = new System.Drawing.Size(66, 17);
            this.radioButtonAnswer2.TabIndex = 2;
            this.radioButtonAnswer2.TabStop = true;
            this.radioButtonAnswer2.Text = "Answer2";
            this.radioButtonAnswer2.UseVisualStyleBackColor = true;
            this.radioButtonAnswer2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.radioButton_ClickEvent);
            // 
            // radioButtonAnswer1
            // 
            this.radioButtonAnswer1.AutoCheck = false;
            this.radioButtonAnswer1.AutoSize = true;
            this.radioButtonAnswer1.Location = new System.Drawing.Point(40, 84);
            this.radioButtonAnswer1.Name = "radioButtonAnswer1";
            this.radioButtonAnswer1.Size = new System.Drawing.Size(66, 17);
            this.radioButtonAnswer1.TabIndex = 1;
            this.radioButtonAnswer1.TabStop = true;
            this.radioButtonAnswer1.Text = "Answer1";
            this.radioButtonAnswer1.UseVisualStyleBackColor = true;
            this.radioButtonAnswer1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.radioButton_ClickEvent);
            // 
            // labelQuestion
            // 
            this.labelQuestion.AutoSize = true;
            this.labelQuestion.Location = new System.Drawing.Point(37, 36);
            this.labelQuestion.Name = "labelQuestion";
            this.labelQuestion.Size = new System.Drawing.Size(49, 13);
            this.labelQuestion.TabIndex = 0;
            this.labelQuestion.Text = "Question";
            // 
            // buttonShowMyScore
            // 
            this.buttonShowMyScore.Location = new System.Drawing.Point(115, 435);
            this.buttonShowMyScore.Name = "buttonShowMyScore";
            this.buttonShowMyScore.Size = new System.Drawing.Size(115, 23);
            this.buttonShowMyScore.TabIndex = 8;
            this.buttonShowMyScore.Text = "Show my Score ...";
            this.buttonShowMyScore.UseVisualStyleBackColor = true;
            this.buttonShowMyScore.Click += new System.EventHandler(this.buttonShowMyScore_Click);
            // 
            // FormQuestionnaire
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(546, 493);
            this.Controls.Add(this.buttonShowMyScore);
            this.Controls.Add(this.panelQuestion);
            this.Name = "FormQuestionnaire";
            this.Text = "Questionnaire";
            this.Load += new System.EventHandler(this.FormQuestionnaire_Load);
            this.panelQuestion.ResumeLayout(false);
            this.panelQuestion.PerformLayout();
            this.ResumeLayout(false);

        }

        

        #endregion

        private System.Windows.Forms.Panel panelQuestion;
        private System.Windows.Forms.Button buttonForward;
        private System.Windows.Forms.Button buttonBackward;
        private System.Windows.Forms.RadioButton radioButtonAnswer4;
        private System.Windows.Forms.RadioButton radioButtonAnswer3;
        private System.Windows.Forms.RadioButton radioButtonAnswer2;
        private System.Windows.Forms.RadioButton radioButtonAnswer1;
        private System.Windows.Forms.Label labelQuestion;
        private System.Windows.Forms.Button buttonShowMyScore;
    }
}

