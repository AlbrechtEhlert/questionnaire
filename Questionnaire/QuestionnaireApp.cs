﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Questionnaire
{
    /// <summary>
    /// Description:    Building and starting of the application
    /// Kata:           https://app.box.com/s/3clcy04py9e1ngfslilv
    /// Author:         Dr. Ehlert / OSZ IMT
    /// Date:           April 2015 / Version2: Mai 2015
    /// </summary>
    
    static class QuestionnaireApp
    {
        /// <summary>
        /// main entry point
        /// </summary>
        
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            // 3-tier-architecture
            Application.Run(new FormQuestionnaire(new QuestionnaireHandler(new FilePersistence("questionnaire.txt"))));
        }
    }

}
