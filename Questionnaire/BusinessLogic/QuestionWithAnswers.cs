﻿using System;
using System.Collections;
using System.Collections.Generic;


namespace Questionnaire
{
    /// <summary>
    /// Description:    Question has three properties
    /// Author:         Dr. Ehlert / OSZ IMT
    /// Date:           April 2015 / Version2: Mai 2015
    /// </summary>

    public class QuestionWithAnswers
    {
        String questionText;
        List<String> listAnswers;
        int correctAnswer;

        public String QuestionText
        {
            get { return questionText; }
            set { questionText = value; }
        }

        public List<String> ListAnswers
        {
            get { return listAnswers; }
            set { listAnswers = value; }
        }

        public int CorrectAnswer
        {
            get { return correctAnswer; }
            set { correctAnswer = value; }
        }

        public QuestionWithAnswers()
        {
            listAnswers = new List<String>();
        }

    }
}
