﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Questionnaire
{
    /// <summary>
    /// Description:    Interface for business logic
    /// Author:         Dr. Ehlert / OSZ IMT
    /// Date:           April 2015 / Version2: Mai 2015
    /// </summary>
    /// 

    public interface IQuestionnaireHandler
    {
        void Reset();
        Boolean IsSolved();
        Boolean IsQuestionnaireFullSolved();
        void SetAnswer(int answerNr);
        int GetGivenAnswerNr();
        int GetCorrectAnswerNr();
        Boolean IsAnswerCorrect();
        String GetQuestion();
        String GetAnswer(int answerNr);
        QuestionWithAnswers GetQuestionWithAnswers();
        int CountQuestions();
        int CountAnswers();
        QuestionWithAnswers GetBackwardQuestion();
        QuestionWithAnswers GetForwardQuestion();
        int CountCorrectAnswers();
        double GetPercentOfCorrectAnswers();
    }
}
