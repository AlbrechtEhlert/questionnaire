﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Questionnaire
{
    /// <summary>
    /// Description:    For the evaluation of the answer sheet
    /// Author:         Dr. Ehlert / OSZ IMT
    /// Date:           April 2015 / Version2: Mai 2015
    /// </summary>
    
    class Evaluation
    {
        private Questionnaire questionnaire;
        private AnswerSheet answerSheet;

        public Evaluation(Questionnaire questionnaire, AnswerSheet answerSheet)
        {
            this.questionnaire = questionnaire;
            this.answerSheet = answerSheet;
        }

        public Boolean IsAnswerCorrect(int questionNr)
        {
            String questionText = questionnaire.GetQuestion(questionNr);
            return (answerSheet.GetGivenAnswerNr(questionNr) == questionnaire.GetCorrectAnswerNr(questionNr));
        }

        public int CountCorrectAnswers()
        {
            int correctAnswers = 0;
            for (int questionNr = 0; questionNr < questionnaire.CountQuestions(); questionNr++)
            {
                if (IsAnswerCorrect(questionNr))
                    correctAnswers++;
            }
            return correctAnswers;
        }

        public double GetPercentOfCorrectAnswers()
        {
            return (double)(((double)CountCorrectAnswers() / (double)questionnaire.CountQuestions())) * (double)100.00;
        }

    }
}
