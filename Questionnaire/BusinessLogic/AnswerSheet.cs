﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Questionnaire
{
    /// <summary>
    /// Description:    The answer sheet contains the answers. It don't know the questionnaire.
    /// CCD:            Only Operation
    /// Author:         Dr. Ehlert / OSZ IMT
    /// Date:           April 2015 / Version2: Mai 2015
    /// </summary>
    
    class AnswerSheet
    {
        private int[] givenAnswers;
        private int maxQuestions;

        public AnswerSheet(int maxQuestions)
        {
            this.maxQuestions = maxQuestions;
            givenAnswers = new int[maxQuestions];
            InitializeAnswers();
        }

        private void InitializeAnswers()
        {
            for (int questionNr = 0; questionNr < maxQuestions; questionNr++)
                givenAnswers[questionNr] = -1;
        }

        public Boolean IsSolved(int questionNr)
        {
            if (givenAnswers[questionNr] == -1)
                return false;
            return true;
        }

        public Boolean IsQuestionnaireFullSolved()
        {
            Boolean fullSolved = true;
            for (int questionNr = 0; questionNr < maxQuestions; questionNr++)
                if (givenAnswers[questionNr] == -1)
                    fullSolved = false;
            return fullSolved;
        }

        public void SetAnswer(int questionNr, int answerNr)
        {
            givenAnswers[questionNr] = answerNr;   // requirements definition: only one answer
        }

        public int GetGivenAnswerNr(int questionNr)
        {
            return givenAnswers[questionNr];
        }

    }
}
