using System;
using System.Collections.Generic;

namespace Questionnaire
{
    /// <summary>
    /// Description:    The questionnaire handler handle the business logic. 
    ///                 The handler connect also the view-tier with the persistence-tier
    /// CCD:            Only Integration                 
    /// Author:         Dr. Ehlert / OSZ IMT
    /// Date:           April 2015 / Version2: Mai 2015
    /// </summary>

    public class QuestionnaireHandler: IQuestionnaireHandler
    {
        private IPersistence persistence;
        private Questionnaire questionnaire;
        private AnswerSheet answerSheet;
        private Evaluation evaluation;
        private int questionNr;                 // Question actuell
                        
        public QuestionnaireHandler(IPersistence persistence)
        {
            this.persistence = persistence;     // reference to the persistence-layer
            questionnaire = new Questionnaire(persistence.GetQuestionsWithAnswers());
            answerSheet = new AnswerSheet(questionnaire.CountQuestions());
            evaluation = new Evaluation(questionnaire, answerSheet);
            Reset();                            // first Question
        }

        public void Reset()
        {
            questionNr = 0;
        }

        public Boolean IsSolved()
        {
            return answerSheet.IsSolved(questionNr);
        }
        
        public Boolean IsQuestionnaireFullSolved()
        {
            return answerSheet.IsQuestionnaireFullSolved();
        }

        public void SetAnswer(int answerNr)
        {
            // requirements definition: only one answer, in this case: the last given answer
            answerSheet.SetAnswer(questionNr, answerNr);
        }

        public int GetGivenAnswerNr()
        {
            return answerSheet.GetGivenAnswerNr(questionNr);
        }

        public int GetCorrectAnswerNr()
        {
            return questionnaire.GetCorrectAnswerNr(questionNr);
        }

        public String GetQuestion()
        {
            return questionnaire.GetQuestion(questionNr);
        }

        public QuestionWithAnswers GetQuestionWithAnswers()
        {
            return questionnaire.GetQuestionWithAnswers(questionNr);
        }

        public String GetAnswer(int answerNr)
        {
            return questionnaire.GetAnswer(questionNr, answerNr);
        }

        public int CountQuestions()
        {
            return questionnaire.CountQuestions();
        }

        public int CountAnswers()
        {
            return questionnaire.CountAnswers(questionNr);
        }

        public QuestionWithAnswers GetBackwardQuestion()
        {
            questionNr = Logic.DecrementQuestionNr(questionNr, 0);
            return questionnaire.GetQuestionWithAnswers(questionNr);
        }

        public QuestionWithAnswers GetForwardQuestion()
        {
            questionNr = Logic.IncrementQuestionNr(questionNr, CountQuestions()-1);
            Console.WriteLine(questionNr);
            return questionnaire.GetQuestionWithAnswers(questionNr);
        }

        public Boolean IsAnswerCorrect()
        {
            return evaluation.IsAnswerCorrect(questionNr);
        }

        public int CountCorrectAnswers()
        {
            return evaluation.CountCorrectAnswers();
        }

        public double GetPercentOfCorrectAnswers()
        {
            return evaluation.GetPercentOfCorrectAnswers();
        }

    }
}