﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Questionnaire
{
    class Logic
    {
        public static int DecrementQuestionNr(int nr, int min)
        {
            if (nr > min)
                nr--;
            return nr;
        }

        public static int IncrementQuestionNr(int nr, int max)
        {
            Console.WriteLine("Vor Inc: " + nr);
            if (nr < max)
                nr++;
            Console.WriteLine("Nach Inc: " + nr);
            return nr;
        }

    }
}
