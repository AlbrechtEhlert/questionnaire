﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Questionnaire
{
    /// <summary>
    /// Description:    The questionnaire contains all questions (include the answers)
    /// CCD:            Only Integration
    /// Author:         Dr. Ehlert / OSZ IMT
    /// Date:           April 2015 / Version2: Mai 2015
    /// </summary>
    
    class Questionnaire
    {
        private List<QuestionWithAnswers> listQuestionWithAnswers;
                        
        public Questionnaire(List<QuestionWithAnswers> listQuestionWithAnswers)
        {
            this.listQuestionWithAnswers = listQuestionWithAnswers;   
        }

        public int GetCorrectAnswerNr(int questionNr)
        {
            return listQuestionWithAnswers[questionNr].CorrectAnswer;
        }

        public String GetQuestion(int questionNr)
        {
            return listQuestionWithAnswers[questionNr].QuestionText;
        }

        public QuestionWithAnswers GetQuestionWithAnswers(int questionNr)
        {
            return listQuestionWithAnswers[questionNr];
        }

        public String GetAnswer(int questionNr, int answerNr)
        {
            QuestionWithAnswers question = listQuestionWithAnswers[questionNr];
            return (String)(question.ListAnswers[answerNr]);
        }

        public int CountQuestions()
        {
            return listQuestionWithAnswers.Count;
        }

        public int CountAnswers(int questionNr)
        {
            return listQuestionWithAnswers[questionNr].ListAnswers.Count;
        }

    }
}
