using System;
using System.IO;
using System.Collections.Generic;

namespace Questionnaire
{
    /// <summary>
    /// Description:    Adapter/Connector to the file and deserializer (text to object list) 
    /// Author:         Dr. Ehlert / OSZ IMT
    /// Date:           April 2015   Version2: Mai 2015
    /// </summary>
    
    public class FilePersistence: IPersistence
    {
        private String filename;

        public FilePersistence(String filename)
        {
            this.filename = filename;
        }

        public List<QuestionWithAnswers> GetQuestionsWithAnswers()
        {
            // Integration
            List<String> listLines = FileIO.ReadStringsFromFile(filename);
            return MapQuestionsWithAnswers(listLines);
        }

        public List<QuestionWithAnswers> MapQuestionsWithAnswers(List<String> listLines)
        {
            // only operation?
            List<QuestionWithAnswers> listQuestions = new List<QuestionWithAnswers>();
            QuestionWithAnswers questionWithAnswers = new QuestionWithAnswers();                  // first question
            Boolean isFirstQuestion = true;
            foreach (String line in listLines)
            {
                if (line.StartsWith("?"))
                {
                    if (isFirstQuestion)
                        isFirstQuestion = false;
                    else
                    {
                        questionWithAnswers.ListAnswers.Add("Don't know");  // for the question before
                        questionWithAnswers = new QuestionWithAnswers();
                    }
                    listQuestions.Add(questionWithAnswers);
                    questionWithAnswers.QuestionText = line.Substring(1);
                }
                else
                {
                    if (line.StartsWith("*"))
                    {
                        questionWithAnswers.ListAnswers.Add(line.Substring(1));
                        questionWithAnswers.CorrectAnswer = questionWithAnswers.ListAnswers.IndexOf(line.Substring(1));
                    }
                    else
                        questionWithAnswers.ListAnswers.Add(line);
                }
            }
            questionWithAnswers.ListAnswers.Add("Don't know");              // for the last quesion
            return listQuestions;
        }

        
    
	}
}
