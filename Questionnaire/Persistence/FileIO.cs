﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Questionnaire
{
    class FileIO
    {
        public static List<String> ReadStringsFromFile(String filename)   
        {
            List<String> listString = new List<String>();
            int counter = 0;
            String line;

            try
            {
                // Read the file and add it line by line.
                System.IO.StreamReader file = new System.IO.StreamReader(filename);
                while ((line = file.ReadLine()) != null)
                {
                    listString.Add(line);
                    counter++;
                }
                file.Close();
            }
            catch (Exception e) { Console.WriteLine(e); }

            return listString;
        }
    }
}
