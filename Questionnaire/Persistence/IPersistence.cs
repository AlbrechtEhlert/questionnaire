using System;
using System.Collections.Generic;

namespace Questionnaire
{
    /// <summary>
    /// Description:    Interface for persistence
    /// Author:         Dr. Ehlert / OSZ IMT
    /// Date:           April 2015 / Version2: Mai 2015
    /// </summary>
    
    public interface IPersistence
    {
        List<QuestionWithAnswers> GetQuestionsWithAnswers(); 
  	}

}
